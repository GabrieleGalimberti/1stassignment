var mail_valid = 0;
function checkmail(){							// controllo dell'input mail lato client
  var duplicate_email = 1
	var k = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;				// Pattern testuale per la email
	var email = document.getElementsByName("emailR")[0].value;
	if(checkEmailList.length > 0){
    for (key=0; key<checkEmailList.length ; key++){
      console.log(checkEmailList[key])
      console.log(email)
			if(checkEmailList[key] == email){
				duplicate_email = 0;
				break;
			}
		}
	}
	if(k.test(email) && email.length < 51 && email.length > 0 && !email.toLowerCase().includes("drop") && !email.toLowerCase().includes("delete") && duplicate_email) {
		mail_valid=1;
		document.getElementById("erroreMail").innerHTML = "";
	}
	else {
		mail_valid=0;
		document.getElementById("erroreMail").innerHTML = "Inserisci una mail valida di massimo 50 caratteri!";  // messaggio di errore
    if(email.length==0){
      document.getElementById("erroreMail").innerHTML = "";
    }
  }
}

var re = /^[a-zA-Z0-9._-]+$/;				// Pattern testuale per nickname, nome, cognome e password

var nick_valid = 0;
function checknick(){							// controllo dell'input nickname lato client
  var duplicate_nick = 1
  var nick = document.getElementsByName("nickname")[0].value;
	if(checkNickList.length > 0){
    for (key=0; key<checkNickList.length ; key++){
      console.log(checkNickList[key])
      console.log(nick)
			if(checkNickList[key] == nick){
				duplicate_nick = 0;
				break;
			}
		}
	}
	if (re.test(nick) && nick.length > 0 && nick.length < 51 && !nick.toLowerCase().includes("drop") && !nick.toLowerCase().includes("delete") && duplicate_nick) { // max 50 caratteri
		nick_valid=1;
		document.getElementById("erroreNick").innerHTML = "";
	}
	else {
		nick_valid=0;
		document.getElementById("erroreNick").innerHTML = "Inserisci un nickname valido di massimo 50 caratteri!";    // messaggio di errore
    if(nick.length==0){
      document.getElementById("erroreNick").innerHTML = "";
    }
  }
}

var name_valid = 0;
function checkname(){							// controllo dell'input nome lato client
  var name = document.getElementsByName("name")[0].value;
	if (re.test(name) && name.length > 0 && name.length < 51 && !name.toLowerCase().includes("drop") && !name.toLowerCase().includes("delete")) { // max 50 caratteri
	  name_valid=1;
		document.getElementById("erroreNome").innerHTML = "";
	}
	else {
		name_valid=0;
		document.getElementById("erroreNome").innerHTML = "Inserisci un nome valido di massimo 50 caratteri!";    // messaggio di errore
    if(name.length==0){
      document.getElementById("erroreNome").innerHTML = "";
    }
	}
}

var surname_valid = 0;
function checksurname(){							// controllo dell'input cognome lato client
  var surname = document.getElementsByName("surname")[0].value;
	if (re.test(surname) && surname.length > 0 && surname.length < 51 && !surname.toLowerCase().includes("drop") && !surname.toLowerCase().includes("delete")) { // max 50 caratteri
		surname_valid=1;
		document.getElementById("erroreCognome").innerHTML = "";
	}
	else {
		surname_valid=0;
		document.getElementById("erroreCognome").innerHTML = "Inserisci un cognome valido di massimo 50 caratteri!";    // messaggio di errore
    if(surname.length==0){
      document.getElementById("erroreCognome").innerHTML = "";
    }
  }
}


var pw_valid = 0;
function checkpw() {							// controllo dell'input password lato client
  var p = document.getElementsByName("pwdR")[0].value;
	if (re.test(p) && p.length > 7 && p.length < 51 && !p.toLowerCase().includes("drop") && !p.toLowerCase().includes("delete")) {
		pw_valid = 1;
		document.getElementById("errorePassword").innerHTML = "";
	}
	else {
		pw_valid = 0;
		document.getElementById("errorePassword").innerHTML = "La password deve avere almeno 8 caratteri ed un massimo di 50 caratteri!";
    if(p.length==0){
      document.getElementById("errorePassword").innerHTML = "";
    }
	}
}

function checksubmit(){
	if(mail_valid && nick_valid && name_valid && surname_valid && pw_valid){  // controllo del submit
		document.getElementById("erroreForm").innerHTML = "";
		return true;
	}
	else {
		document.getElementById("erroreForm").innerHTML = "Per favore risolvi gli errori prima di registrarti!";    // messaggio di errore
		return false;
	}
}
