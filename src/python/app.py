from flask import Flask, render_template, request, url_for, flash, redirect, session, json
import mysql.connector
import json

app = Flask(__name__)
app.config['SECRET_KEY'] = '0742df9b0f132ad49e110398d635a39c'
app.config['TESTING'] = True
app.config['DEBUG'] = True

# creazione del database
db = mysql.connector.connect( user='root', password='password',host='0.0.0.0', port=3306, database='accounts')
mycursor = db.cursor()
mycursor.execute("create database IF NOT EXISTS accounts;");
mycursor.execute("create table IF NOT EXISTS users (idU integer AUTO_INCREMENT PRIMARY KEY,nickname varchar(50) Unique NOT NULL,e_mail varchar(50) Unique NOT NULL,nome varchar(50) NOT NULL,cognome varchar(50) NOT NULL,password varchar(50) NOT NULL, numero integer NOT NULL DEFAULT 0);")
mycursor.close();

# home page
@app.route("/")
def home():
    session.pop('logged_in', None)
    cursor = db.cursor()
    cursor.execute("""SELECT e_mail , nickname FROM users;""")
    myresult = cursor.fetchall()
    cursor.close()
    nickList = []
    emailList = []
    if len(myresult)>0 :
        for t in myresult :
            emailList.append(t[0].encode('utf8'))
            nickList.append(t[1].encode('utf8'))
    return render_template('template.html', nL = nickList , eL = emailList )

# metodo per la verifica delle credenziali di accesso di uno user
# l'accesso di uno user comporta la creazione di una sessione
@app.route("/signin",methods=["GET","POST"])
def signin():
    if request.method == "POST":
        _email = request.form['emailL']
        _pwd = request.form['pwdL']
        flag = False
        cursor = db.cursor()
        cursor.execute("""SELECT e_mail , password , idU FROM users WHERE e_mail = %s AND password = %s ;""", (_email,_pwd))
        myresult = cursor.fetchall()
        cursor.close()
        if len(myresult)>0 :
            if (myresult[0][0] == _email) & (myresult[0][1] == _pwd) :
                flag = True
                session['logged_in'] = myresult[0][2]
    if flag :
        return redirect(url_for("application"))
    else :
        session.pop('_flashes', None)
        flash('Something went wrong, control your email or your password!')
    return redirect(url_for("home"))

# metodo per la verifica di un account esistente e per la creazione di un nuovo account
@app.route("/signup",methods=['GET','POST'])
def signup():
    if request.method == "POST":
        nickR = request.form['nickname']
        nameR = request.form['name']
        surnameR = request.form['surname']
        emailR = request.form['emailR']
        pwdR = request.form['pwdR']
        cursor = db.cursor()
        cursor.execute("""SELECT nickname , e_mail FROM users WHERE e_mail = %s OR nickname = %s;""", (emailR,nickR))
        myresult=cursor.fetchall()
        if len(myresult)>0 :
            if (myresult[0][0] == nickR) | (myresult[0][1] == emailR) :
                flash('Something went wrong, control your email or your password!')
                return redirect(url_for("home"))
        cursor.execute("""insert into users (nickname,e_mail,nome,cognome,password) values (%s,%s,%s,%s,%s);""", (nickR,emailR,nameR,surnameR,pwdR))
        db.commit()
        cursor.close()
        session.pop('_flashes', None)
        flash('Thank You For Sign Up!')
    return redirect(url_for("home"))

# metodo per la cancellazione della sessione
@app.route('/logout',methods=['GET','POST'])
def logout():
    session.pop('logged_in', None)
    return redirect(url_for("home"))

# gestione della pagina riservata con restituzione del numero di visite della pagina
@app.route('/application')
def application():
    try:
        if (session['logged_in'] != None):
            account = session['logged_in']
            cursor = db.cursor()
            cursor.execute("""UPDATE users SET  numero = numero +1 WHERE idU = %s ;""", (account,))
            db.commit()
            cursor.execute("""SELECT nickname, numero FROM users WHERE idU = %s ;""", (account,))
            myresult = cursor.fetchall()
            nicknameAccount = myresult[0][0]
            numberOfAccess = myresult[0][1]
            stringa = ''
            cursor.close()
            if numberOfAccess > 1 :
                stringa = str(numberOfAccess) + ' volte'
            else :
                stringa = str(numberOfAccess) + ' volta'
            return render_template('index.html', nk=nicknameAccount , na=stringa )
    except KeyError :
        return redirect(url_for("home"))
    return redirect(url_for("logout"))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True, port=80)
